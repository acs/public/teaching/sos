/// Example is derived from tiny-http example
/// https://github.com/tiny-http/tiny-http/blob/master/examples/hello-world.rs
extern crate tiny_http;

fn main() {
	let server = tiny_http::Server::http("[::]:9975").unwrap();
	println!("Now listening on port 9975");

	for rq in server.incoming_requests() {
		let now = time::OffsetDateTime::now_utc();
		let text = format!(
			"Hello World!\nThe current UTC time is {}!\n",
			now
		);
		let response = tiny_http::Response::from_string(text);
		let _ = rq.respond(response);
	}
}
