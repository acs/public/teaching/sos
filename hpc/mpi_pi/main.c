#include <mpi.h>
#include <stdio.h>

/* Define pi to see how accurate our computation is. */
#define MY_PI 3.141592653589793238462643

int main(int argc, char** argv) {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get current wall clock time
    double time1 = MPI_Wtime();

    long long i, num_steps = 100000000;
    double step = 1.0 / (double) num_steps;
    double x, sum = 0.0;

    long long count = num_steps / world_size;
    long long start = count * world_rank;
    long long end = count * world_rank + count;

    for (i = start; i < end; i++) {
        x = (i + 0.5) * step;
        sum += 4.0 / (1.0 + x * x);
    }

    double total_sum = 0.0;
    MPI_Reduce(&sum, &total_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    double time2 = MPI_Wtime();

    if (world_rank == 0) {
        double pi = step * total_sum;
	printf("Pi:       %f\n", pi);
	printf("Accuracy: %.10lf\n", MY_PI - pi);
	printf("Time:     %.10lf seconds\n", time2 - time1);
    }

    // Finalize the MPI environment.
    MPI_Finalize();
}
