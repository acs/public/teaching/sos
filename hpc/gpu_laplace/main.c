#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <omp.h>

#define COLUMNS   1024
#define ROWS      1024
#define MAX_ITERATIONS  1000

#define TOLERANCE 0.01

double A_new[ROWS+2][COLUMNS+2];
double A[ROWS+2][COLUMNS+2];

int main(int argc, char** argv) {
    int iterations=0;

    printf("Check OMP devices %d\n", omp_get_num_devices());

    // Initialize A
    for (int i = 0; i <= ROWS+1; i++) {
        for (int j = 0; j <= COLUMNS+1; j++) {
            A[i][j] = 0.0;
        }
    }

    // Local boundry condition endpoints
    double tMin = 0.0;
    double tMax = 100.0;
    double dA=100.0;

    // Left and right boundaries
    for (int i = 0; i <= ROWS+1; i++) {
        A[i][0] = 0.0;
        A[i][COLUMNS+1] = tMin + ((tMax-tMin)/ROWS)*i;
    }

    // Top boundary
    for (int j = 0; j <= COLUMNS+1; j++)
        A[0][j] = 0.0;

    // Bottom boundary
    for (int j=0; j<=COLUMNS+1; j++)
	A[ROWS+1][j] = (100.0/COLUMNS) * j;

    printf("Start calculation\n");

    // Get current wall clock time
    double tstart = omp_get_wtime();

    // If map-type is to or tofrom, this new item is initialized with the value
    // of the original list item in list in the host data environment.
    // If map-type is from or alloc, the initial value of the list item in the
    // device data environment is undefined.
    #pragma omp target data map(alloc:A_new) map(tofrom:A)
    while (dA > TOLERANCE && iterations < MAX_ITERATIONS) {
        dA = 0.0;
        iterations++;

	#pragma omp target teams distribute parallel for reduction(max:dA) map(tofrom:dA)
        for (int i = 1; i <= ROWS; i++) {
            for(int j = 1; j <= COLUMNS; j++) {
                A_new[i][j] = 0.25 * (A[i+1][j] + A[i-1][j] + A[i][j+1] + A[i][j-1]);
		dA = fmax(dA, fabs(A_new[j][i] - A[j][i]));
            }
        }

	#pragma omp target teams distribute parallel for
	for (int j = 1; j <= ROWS; j++) {
	    for (int i = 1; i <= COLUMNS; i++ ) {
		A[j][i] = A_new[j][i];
            }
        }
    }

    double tend = omp_get_wtime();

    printf("Max error at iteration %d was %lf\n", iterations-1, dA);
    printf("Total time was %lf seconds.\n", tend - tstart);

    return 0;
}
