# Demo SLURM script

Slurm script to show the processor configuration of the HPC cluster:

To submit the SLURM job use the following command:

```sh
$ sbatch slurm.job
```
