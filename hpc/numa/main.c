#include <numa.h>
#include <stdio.h>

int main(void) {
        if (numa_available() < 0) {
                fprintf(stderr, "System does not support NUMA API!\n");
        }

        int n = numa_max_node();
        int size = 1024*1024;
        printf("There are %d nodes on your system\n", n + 1);

	void *mem = numa_alloc_onnode(size, n);
	if (mem == NULL) {
                fprintf(stderr, "could not allocate memory on node %d!\n", n);
		exit(EXIT_FAILURE);
        }

        if (numa_run_on_node(n) != 0) {
                fprintf(stderr, "could not assign current thread to node %d!\n", n);
		exit(EXIT_FAILURE);
        }

        // use data on node n
        memset(mem, 0xFF, size);

        numa_free(mem, size);

        return 0;
}
