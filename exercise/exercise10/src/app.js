const app = require("express")();
const id = process.env.APPID;

app.get("/", (req, res)=> {
    res.send(`Hello World from Server ${id}!`);
});

app.listen(3000, function () {
    console.log("app listening on port 3000");
});